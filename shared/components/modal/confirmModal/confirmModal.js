const html = (id) => `

<div id=${id} class="modal confirmModal hidden">
    <div class="header">
    <h3>Are You Sure?</h3>
    <button type="button" class="btn btn-close cancelBtn">⨉</button>
    </div>
    <main class="body">
        <p class="message"></p>
    </main>
    <footer>
        <button type="button" class="btn btn-success successBtn">Yes</button>
        <button type="button" class="btn btn-cancle cancelBtn">Cancel</button>
    </footer>
</div>
<div id="overlay" class=""></div>

`

class ConfirmModal extends HTMLElement {
    constructor() {
        super()
        this.loadHtml()
        this.modalClassList = document.querySelector(".confirmModal").classList
        this.overlayClassList = document.querySelector("#overlay").classList
        this.SuccessFunc = this.SuccessFunc.bind(this)
        this.CancelFunc = this.CancelFunc.bind(this)
    }

    loadHtml() {
        document.body.innerHTML += html("ConfirmModal")
    }

    SuccessFunc() {
        if (this.onSuccess)
            this.onSuccess()
        this.Close()
    }

    CancelFunc() {
        if (this.onCancel)
            this.onCancel()
        this.Close()
    }

    Open(onSuccess, onCancel, msg) {
        document.querySelector(".confirmModal .message").innerHTML = msg || "Are You Sure?"
        this.onSuccess = onSuccess
        this.onCancel = onCancel
        this.modalClassList.remove("hidden")
        this.overlayClassList.add("overlay")
        document.querySelectorAll(".confirmModal .successBtn").forEach(ele => ele.addEventListener("click", this.SuccessFunc))
        document.querySelectorAll(".confirmModal .cancelBtn").forEach(ele => ele.addEventListener("click", this.CancelFunc))
    }

    Close() {
        this.modalClassList.add("hidden")
        this.overlayClassList.remove("overlay")
        document.querySelectorAll(".confirmModal .successBtn").forEach(ele => ele.removeEventListener("click", this.SuccessFunc))
        document.querySelectorAll(".confirmModal .cancelBtn").forEach(ele => ele.removeEventListener("click", this.CancelFunc))
    }
}

customElements.define('confirm-modal', ConfirmModal);