function modalEnd(status) {
    var res = `You just clicked "${status ? "Yes" : "Cancel"}"`
    document.querySelector(".result").innerHTML = res
}
function init() {
    var confirmModal = document.querySelector('confirm-modal');
    document.querySelector("#confirmBtn").addEventListener("click", () => confirmModal.Open(() => modalEnd(true), () => modalEnd(false), "Are you sure you want to Continue?"))
}
window.onload = () => init()